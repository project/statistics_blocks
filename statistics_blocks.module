<?php

/**
 * @file
 * Shows Drupal statistics and dblog output as blocks.
 */

include_once DRUPAL_ROOT . '/modules/dblog/dblog.admin.inc';

/**
 * Implements hook_block_info().
 */
function statistics_blocks_block_info() {
  $blocks = array();

  $blocks['recent']['info'] = t('Recent log messages');
  $blocks['recent']['cache'] = DRUPAL_NO_CACHE;

  $blocks['not-found']['info'] = t("Top 'page not found' errors");
  $blocks['not-found']['cache'] = DRUPAL_NO_CACHE;

  $blocks['access-denied']['info'] = t("Top 'access denied' errors");
  $blocks['access-denied']['cache'] = DRUPAL_NO_CACHE;

  // only enable these blocks when statistics is active
  if (variable_get('statistics_count_content_views', 0)) {
    $blocks['hits']['info'] = t('Recent hits');
    $blocks['hits']['cache'] = DRUPAL_NO_CACHE;

    $blocks['pages']['info'] = t('Top pages');
    $blocks['pages']['cache'] = DRUPAL_NO_CACHE;

    $blocks['referrers']['info'] = t('Top referrers');
    $blocks['referrers']['cache'] = DRUPAL_NO_CACHE;
  }
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function statistics_blocks_block_configure($delta = '') {
  $numbers = array(drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30, 40)));
  // TODO: Customize the strings copied from statistics module some day.
  $form['statistics_blocks_' . $delta] = array('#type' => 'select', '#title' => t('Number of most recent views to display'), '#default_value' => variable_get('statistics_blocks_' . $delta, 10), '#options' => $numbers, '#description' => t('How many content items to display in "recently viewed" list.'));
  return $form;
}

/**
 * Implements hook_block_save().
 */
function statistics_blocks_block_save($delta = '', $edit = array()) {
  variable_set('statistics_blocks_' . $delta, $edit['statistics_blocks_' . $delta]);
}

/**
 * Implements hook_block_view().
 */
function statistics_blocks_block_view($delta = '') {
  if (user_access('access content')) {
    switch ($delta) {
  
      case 'recent':
        $block['subject'] = t('Recent log messages');
        $block['content'] = dblog_overview();
        break;
  
      case 'not-found':
        $block['subject'] = t("Top 'page not found' errors");
        $block['content'] = dblog_top('page not found');
        break;
  
      case 'access-denied':
        $block['subject'] = t("Top 'access denied' errors");
        $block['content'] = dblog_top('access denied');
        break;
  
      case 'hits':
        $block['subject'] = t('Recent hits');
        $block['content'] = _statistics_blocks_content_hits();
        break;
  
      case 'pages':
        $block['subject'] = strip_tags(t('Top pages in the past %interval', array('%interval' => format_interval(variable_get('statistics_flush_accesslog_timer', 259200)))));
        $block['content'] = _statistics_blocks_content_pages();
        break;
  
      case 'referrers':
        $block['subject'] = strip_tags(t('Top referrers in the past %interval', array('%interval' => format_interval(variable_get('statistics_flush_accesslog_timer', 259200)))));
        $block['content'] = _statistics_blocks_content_referrers();
        break;
    }
    return $block;
  }
}

// copied from module/statistics/statistics.admin.inc
//   removed calls to drupal_set_title() -- maybe use $saved_title=drupal_set_title()
//   TODO: change query limit to var

function _statistics_blocks_content_hits() {
  $header = array(
    array('data' => t('Timestamp'), 'field' => 'a.timestamp', 'sort' => 'desc'),
    array('data' => t('Page'), 'field' => 'a.path'),
    array('data' => t('User'), 'field' => 'u.name'),
    array('data' => t('Operations'))
  );

  $query = db_select('accesslog', 'a', array('target' => 'slave'))->extend('PagerDefault')->extend('TableSort');
  $query->join('users', 'u', 'a.uid = u.uid');
  $query
    ->fields('a', array('aid', 'timestamp', 'path', 'title', 'uid'))
    ->fields('u', array('name'))
    ->limit(variable_get('statistics_blocks_hits'))
    ->orderByHeader($header);

  $result = $query->execute();
  $rows = array();
  foreach ($result as $log) {
    $rows[] = array(
      array('data' => format_date($log->timestamp, 'short'), 'class' => array('nowrap')),
      _statistics_format_item($log->title, $log->path),
      theme('username', array('account' => $log)),
      l(t('details'), "admin/reports/access/$log->aid"));
  }

  $build['statistics_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No statistics available.'),
  );
  $build['statistics_pager'] = array('#theme' => 'pager');
  return $build;
}


function _statistics_blocks_content_pages() {
  $header = array(
    array('data' => t('Hits'), 'field' => 'hits', 'sort' => 'desc'),
    array('data' => t('Page'), 'field' => 'path')
  );

  $query = db_select('accesslog', 'a', array('target' => 'slave'))->extend('PagerDefault')->extend('TableSort');
  $query->addExpression('COUNT(path)', 'hits');
  // MAX(title) avoids having empty node titles which otherwise causes
  // duplicates in the top pages list.
  $query->addExpression('MAX(title)', 'title');

  $query
    ->fields('a', array('path'))
    ->groupBy('path')
    ->limit(variable_get('statistics_blocks_pages'))
    ->orderByHeader($header);

  $count_query = db_select('accesslog', 'a', array('target' => 'slave'));
  $count_query->addExpression('COUNT(DISTINCT path)');
  $query->setCountQuery($count_query);

  $result = $query->execute();
  $rows = array();
  foreach ($result as $page) {
    $rows[] = array($page->hits, _statistics_format_item($page->title, $page->path));
  }

  $build['statistics_top_pages_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No statistics available.'),
  );
  $build['statistics_top_pages_pager'] = array('#theme' => 'pager');
  return $build;
}

function _statistics_blocks_content_referrers() {
  $header = array(
    array('data' => t('Hits'), 'field' => 'hits', 'sort' => 'desc'),
    array('data' => t('Url'), 'field' => 'url'),
    array('data' => t('Last visit'), 'field' => 'last'),
  );
  $query = db_select('accesslog', 'a')->extend('PagerDefault')->extend('TableSort');

  $query->addExpression('COUNT(url)', 'hits');
  $query->addExpression('MAX(timestamp)', 'last');
  $query
    ->fields('a', array('url'))
    ->condition('url', '%' . $_SERVER['HTTP_HOST'] . '%', 'NOT LIKE')
    ->condition('url', '', '<>')
    ->groupBy('url')
    ->limit(variable_get('statistics_blocks_referrers'))
    ->orderByHeader($header);

  $count_query = db_select('accesslog', 'a', array('target' => 'slave'));
  $count_query->addExpression('COUNT(DISTINCT url)');
  $count_query
    ->condition('url', '%' . $_SERVER['HTTP_HOST'] . '%', 'NOT LIKE')
    ->condition('url', '', '<>');
  $query->setCountQuery($count_query);

  $result = $query->execute();
  $rows = array();
  foreach ($result as $referrer) {
    $rows[] = array($referrer->hits, _statistics_link($referrer->url), t('@time ago', array('@time' => format_interval(REQUEST_TIME - $referrer->last))));
  }

  $build['statistics_top_referrers_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No statistics available.'),
  );
  $build['statistics_top_referrers_pager'] = array('#theme' => 'pager');
  return $build;
}
